from django.urls import path, re_path
from .views import AllReportsView, SingleReportView, LoginView, update_report

app_name = 'webpanels'

urlpatterns = [
    path('reports/<int:pk>/', update_report, name='report_details'),
    # path('reports/<int:pk>/edit', UpdateReportView.as_view(), name='report_update'),
    path('', AllReportsView.as_view(), name='reports'),
]