from django.views.generic import ListView, TemplateView, DetailView, FormView
from reports.models import Report, ReportImage
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import get_object_or_404, render
from .forms import ReportForm, CommentForm


class DashBoardView(TemplateView):
    template_name = "dashboard_home.html"


class LoginView(TemplateView):
    template_name = "login.html"


class AllReportsView(ListView):
    model = Report
    template_name = "dashboard_home.html"

class SingleReportView(DetailView):
    model = Report
    template_name = "webpanels/report_detail.html"


def update_report(request, pk):
    report = Report.objects.get(pk=pk)
    # report = get_object_or_404(Report, pk=pk)
    form_report = ReportForm(instance=report)
    form_comment = CommentForm(request.POST or None)
    if request.method == 'POST':
        form_report = ReportForm(request.POST, instance=report)
        if form_report.is_valid():
            updated_report = form_report.save()
        if form_comment.is_valid():
            new_comment = form_comment.save()
        if update_report and new_comment:
            return HttpResponseRedirect(reverse("webpanels:reports"))
    
    context = {
        'form_report': form_report,
        'form_comment': form_comment
    }
    return render(request, 'webpanels/report_detail.html', context)


# class UpdateReportView(FormView):
#     model = Report
#     template_name = "webpanels/report_detail.html"
#     form_class = ReportForm

#     def get_form_kwargs(self):
#         form_kwargs = super(UpdateReportView, self).get_form_kwargs()
#         if 'pk' in self.kwargs:
#             form_kwargs['initial'] = get_object_or_404(Report, pk=int(self.kwargs['pk']))
#         return form_kwargs

#     def post(self, request, *args, **kwargs):
#         form = self.get_form()
#         print(form)
#         return HttpResponseRedirect(reverse("webpanels:reports"))
        