from django import forms
from reports.models import Report, Comment


CHOICES = [(True, 'Важное'), (False, 'Обычное')]


class ReportForm(forms.ModelForm):
	title = forms.CharField(max_length = 150)
	text = forms.CharField(max_length = 600)
	location = forms.CharField(max_length = 150)
	importance = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES)


	class Meta:
		model = Report
		fields = ('title', 'text', 'location', 'importance')


class CommentForm(forms.ModelForm):
	
	class Meta:
		model = Comment
		fields = ('text',)
	# comment = forms.CharField(max_length = 350)