# from django.contrib.gis.db.models import PointField
from django.db import models
from django.contrib.auth.models import User


class Report(models.Model):
	title = models.CharField(max_length = 150)
	text = models.CharField(max_length = 600)
	# location_latlng = PointField() # for postgres
	location = models.JSONField(default=str)
	importance = models.BooleanField(default=False)
	rank = models.IntegerField(default=0)
	done = models.BooleanField(default=True)
	created_at = models.DateTimeField(auto_now_add=True)
	edited_at = models.DateTimeField(auto_now=True)
	

	def __str__(self):
		return f"{self.pk} - {self.title}"


class ReportImage(models.Model):
	img = models.ImageField(upload_to='report_images')
	report = models.ForeignKey(Report, on_delete=models.CASCADE)
	

class Comment(models.Model):
	author = models.ForeignKey(User, on_delete=models.CASCADE)
	text = models.CharField(max_length = 350)
	report = models.ForeignKey(Report, on_delete=models.CASCADE, related_name='comments')
	
	
	