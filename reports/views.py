from .models import Report, ReportImage, Comment
from rest_framework import generics
from .serializers import ReportSerializer, CommentSerializer, VoteSerializer
from rest_framework.response import Response
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from django.contrib.auth.models import User
from rest_framework.parsers import MultiPartParser, JSONParser
from django.db.models import F



class CommentCreateView(generics.CreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def perform_create(self, serializer):
        serializer.save(author=User.objects.first(), report=Report.objects.get(pk=self.kwargs.get('report_id')))


class ReportsViewSet(viewsets.ModelViewSet):
    serializer_class = ReportSerializer
    queryset = Report.objects.all()
    parser_classes = (MultiPartParser, JSONParser)


    @action(detail=True, methods=['post'], serializer_class=VoteSerializer)
    def set_vote(self, request, pk=None):
        report = self.get_object()
        user = User.objects.get(pk=1)
        serializer = VoteSerializer(data=request.data)
        if serializer.is_valid():
            report.rank = F('rank') + serializer.validated_data['vote']
            report.save()
            return Response({'status': 'vote is set'})
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)