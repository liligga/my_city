from rest_framework import serializers
from .models import Report, Comment
from django.contrib.auth.models import User



class AuthorSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ('username',)


class CommentSerializer(serializers.ModelSerializer):
	author = AuthorSerializer(read_only=True)

	class Meta:
		model = Comment
		fields = ('id', 'author', 'text')


class ReportSerializer(serializers.ModelSerializer):
	comments = CommentSerializer(many=True, read_only=True)

	class Meta:
		model = Report
		fields = ['id', 'title', 'text', 'location', 'importance', 'rank', 'done', 'created_at', 'comments']


# class ReportSerializer(serializers.Serializer):
# 	id = serializers.IntegerField(read_only=True)
# 	title = serializers.CharField(max_length=150)
# 	text = serializers.CharField(max_length=600)
# 	location = serializers.JSONField()
# 	importance = serializers.BooleanField(read_only=True)
# 	rank = serializers.IntegerField(read_only=True)
# 	done = serializers.BooleanField(read_only=True)
# 	img = serializers.ImageField(required=False)
# 	created_at = serializers.DateTimeField(read_only=True)
# 	comments = CommentSerializer(many=True, read_only=True)

# 	def create(self, validated_data):
#         instance = Report(**validated_data)
# 		instance.reportimages_set.add(validated_data.get('img')


class VoteSerializer(serializers.Serializer):
	vote = serializers.IntegerField(max_value=1, min_value=-1)

