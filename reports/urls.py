from django.urls import path, re_path
from rest_framework.routers import DefaultRouter
from .views import ReportsViewSet, CommentCreateView

app_name = 'api'
router = DefaultRouter()
router.register(r'reports', ReportsViewSet, basename='reports')


# urlpatterns = [
#     path('reports/', ReportsListView.as_view(), name='report_list'),
# ]
urlpatterns = router.urls
urlpatterns += [
    path('coments/<int:report_id>', CommentCreateView.as_view(), name='comment_create'),
]