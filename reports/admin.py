from django.contrib import admin
from .models import Report, ReportImage, Comment


admin.site.register(Report)
admin.site.register(Comment)
admin.site.register(ReportImage)
# admin.site.register(ReportCategory)

